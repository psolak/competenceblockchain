import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudentSearchComponent} from "./student-search/student-search.component";
import {StudentInfoComponent} from "./student-info/student-info.component";

const routes: Routes = [
  {path: 'user-search', component: StudentSearchComponent},
  {path: 'student-info', component: StudentInfoComponent},
  {path: '', redirectTo: 'user-search', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
