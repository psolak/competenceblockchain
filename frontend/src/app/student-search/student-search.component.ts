import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {Student} from "../student";

@Component({
  selector: 'app-student-search',
  templateUrl: './student-search.component.html',
  styleUrls: ['./student-search.component.scss']
})
export class StudentSearchComponent {

  constructor(private router: Router, private http: HttpClient) {
  }

  public searchStudent(student: string) {
    this.http.get<Student>(`api/student/${student}`)
      /* .pipe(catchError(() => {
        return of({
          batch: 2023,
          id: 23050,
          name: "Hugo",
          public_key: "erd180jt9z7tuy52rjvuplf68yfsty80u2amw4f4syggwhswm6yn0juqx8trs5",
          surname: "Belot-Deloro"
        })
      })) */
      .subscribe((student: Student) => this.router.navigate(['/student-info', {student: JSON.stringify(student)}]));
  }

}
