import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Student} from "../student";
import {HttpClient} from "@angular/common/http";

// const API = "https://api.elrond.com"
const API = "https://devnet-api.elrond.com"

@Component({
  selector: 'app-student-info',
  templateUrl: './student-info.component.html',
  styleUrls: ['./student-info.component.scss']
})
export class StudentInfoComponent implements OnInit {

  student: Student | null = null;

  skills: SkillDesc[] = [];

  constructor(private route: ActivatedRoute, private http: HttpClient) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(({student}) => {
      this.student = JSON.parse(student);
      this.http.get<NFT[]>(`${API}/accounts/${this.student?.public_key}/nfts`)
        .subscribe(nfts => {
          nfts.forEach(nft => {
            this.http.get<SkillDesc>(nft.url)
              .subscribe(json => {
                this.skills.push(json);
              });
          })
        });
    });
  }
}

interface NFT {
  url: string;
}

interface SkillDesc {
  image: string;
  name: string;
  description: string;
}
