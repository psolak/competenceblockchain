export interface Student {
  name: string,
  surname: string,
  id: number,
  batch: number
  public_key: string,
}
