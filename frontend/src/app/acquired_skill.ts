export interface AcquiredSkill {
  student_id: number,
  skill_id: number,
  date: Date,
}
