import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { MyErrorHandler } from './http-config';


export interface Student {
  id: number,
  firstName: string,
  lastName: string,
  login: string,
  promo: number,
  publicKey: number
}

export interface Competence {
  id: number,
  name: string,
  description: string,
  studentId: number
}

export interface AddCompetencesRequest {
  competenceIds: number[],
  studentId: number
}

@Injectable({
  providedIn: 'root'
})

export class StudentCompetenceService {

  apiUrl="api"
  constructor(private http: HttpClient, private errorHandler: MyErrorHandler) { }

  getStudentFromLogin(login: string): Observable<Student> {
    return this.http.get<Student>(`${this.apiUrl}/student/${login}`).pipe(catchError(this.errorHandler.handle));
  }

  getCompetenceFromName(competenceName: string): Observable<Competence> {
    return this.http.get<Competence>(`${this.apiUrl}/competence/${competenceName}`).pipe(catchError(this.errorHandler.handle));
  }

  getAllCompetences(): Observable<Competence[]> {
    return this.http.get<Competence[]>(`${this.apiUrl}/competence`).pipe(catchError(this.errorHandler.handle));
  }

  addCompetencesToStudent(competencesIds: number[], studentId: number) {
    const request: AddCompetencesRequest = {competenceIds: competencesIds, studentId: studentId};
    console.log(request)
    this.http.post<any>(`${this.apiUrl}/student/add`, request).pipe(catchError(this.errorHandler.handle)).subscribe()
  }
}
