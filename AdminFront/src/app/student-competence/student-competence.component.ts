import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { StudentCompetenceService, Student, Competence } from '../student-competence.service';

@Component({
  selector: 'app-student-competence',
  templateUrl: './student-competence.component.html',
  styleUrls: ['./student-competence.component.scss']
})
export class StudentCompetenceComponent implements OnInit {

  student?: Student;
  all_competences?: Competence[];

  competenceForm: FormGroup;

  studentForm = new FormGroup({
    login: new FormControl(''),
  });

  constructor(fb: FormBuilder, private studentService: StudentCompetenceService) {
    this.studentService.getAllCompetences().subscribe(
      competences => this.all_competences = competences
    )
    this.competenceForm = fb.group({
      selectedIds: new FormArray([])
    });
  }

  onSubmit() {
    this.studentService.getStudentFromLogin(this.studentForm.get('login')?.value).subscribe(
      stud => this.student = stud
    )
  }

  onCheckboxChange(event: any) {
    const selectedCompetences = (this.competenceForm.controls?.['selectedIds'] as FormArray);
    if (event.target.checked) {
      selectedCompetences.push(new FormControl(event.target.value));
    } else {
      const index = selectedCompetences.controls
      .findIndex(x => x.value === event.target.value);
      selectedCompetences.removeAt(index);
    }
  }

  submit() {
    if (this.student){
     this.studentService.addCompetencesToStudent(this.competenceForm.controls?.['selectedIds'].value, this.student.id)
    }
  }

  ngOnInit(): void {
  }
}
