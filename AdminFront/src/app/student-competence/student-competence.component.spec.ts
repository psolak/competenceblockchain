import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentCompetenceComponent } from './student-competence.component';

describe('StudentCompetenceComponent', () => {
  let component: StudentCompetenceComponent;
  let fixture: ComponentFixture<StudentCompetenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentCompetenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentCompetenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
