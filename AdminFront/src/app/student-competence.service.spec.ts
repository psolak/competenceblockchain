import { TestBed } from '@angular/core/testing';

import { StudentCompetenceService } from './student-competence.service';

describe('StudentCompetenceService', () => {
  let service: StudentCompetenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentCompetenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
