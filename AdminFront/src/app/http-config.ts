import { HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { EMPTY } from "rxjs";

@Injectable({
    providedIn: 'root'
})

export class MyErrorHandler {
    handle(error: HttpErrorResponse) {
        if (error.status === 0) {
            window.alert('An error occured: ' + error.error)
        }
        else {
            window.alert(`Backend returned code ${error.status}, body was ${error.error}`);
        }
        return EMPTY;
    }
}