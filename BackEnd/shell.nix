  { pkgs ? import <nixpkgs> {} }:

  pkgs.mkShell {
    packages = with pkgs; [
      jdk
      maven
    ];
  }
