import matplotlib.pyplot as plt
from PIL import Image
import cairosvg
from geopatterns import GeoPattern
pattern = GeoPattern('TOm a son semestre.', generator='xes')
cairosvg.svg2png(bytestring=pattern.svg_string, write_to="output.png", scale=10)