import os, sys, csv

from pathlib import Path
import cv2


def generate_images(csv_path, image_path, output_path):
    """
    Args:
        csv_path (str) : path of the CSV with one word per line
        image_path (str) : path of the background image
        output_path (str) : path of the folder to store created images
    """
    csv_path = Path(csv_path)
    image_path = Path(image_path)
    output_path = Path(output_path)

    base_img = cv2.imread(str(image_path))

    output_path.mkdir(parents=True, exist_ok=True)

    with open(csv_path, 'r') as csvfile:
        reader = csv.reader(csvfile)

        for row in reader:
            word = str(row[0])
            img = base_img.copy()

            # get boundary of this text
            textsize = cv2.getTextSize(word, cv2.FONT_HERSHEY_SIMPLEX, 1, 2)[0]

            # get coords based on boundary
            textX = (img.shape[1] - textsize[0]) // 2
            textY = (img.shape[0] + textsize[1]) // 2

            cv2.putText(img, word, (textX, textY), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 1, 2)
            cv2.imwrite(str(output_path / f"{word}.png"), img)


if __name__ == "__main__":
    csv_path = sys.argv[1]
    image_path = sys.argv[2]
    output_path = sys.argv[3]
    generate_images(csv_path, image_path, output_path)
