#!/bin/sh
IMAGE_PATH=$1
WALLET_PATH="target/classes/scripts/ImageGeneration/Final/arweave-keyfile-IhgQ_eq1r5-2aokioZuuGbQg4nTguYlz_6LxULCCnR0.json"
FOLDER_2023_ID="d809a174-3d35-4b9f-81d1-086e8de09c86"
#ardrive get-balance -w $WALLET_PATH

#list every file of the folder
#ardrive list-folder --parent-folder-id $FOLDER_2023_ID | jq '[ .[] | select(.entityType == "file") | [.name, "https://arweave.net/" + .dataTxId ]]' | tee competence_list.json

#Number of operation on the blockchain
TxCount=$( ardrive get-mempool | jq 'length' )
FeeBoost=1
if [ $TxCount -gt 50 ]
then
    FeeBoost=1.1
fi
if [ $TxCount -gt 1000 ]
then
    FeeBoost=1.5
fi

echo "Congestion =" $TxCount ", Fee boost = " $FeeBoost

#upload
ardrive upload-file --skip --local-file-path $IMAGE_PATH --parent-folder-id $FOLDER_2023_ID -w $WALLET_PATH --boost $FeeBoost --dry-run
#| jq '[ .created[] | .dataTxId ]' > $1"_dataTxId_tmp.json"
#ardrive upload-file --ask --local-file-path $IMAGE_PATH --parent-folder-id $FOLDER_2023_ID -w $WALLET_PATH --boost $FeeBoost --dry-run