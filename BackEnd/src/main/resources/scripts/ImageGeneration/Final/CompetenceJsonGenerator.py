#!/bin/python

import os
import sys
import json

def main():
    # [student_id, student_surname, student_name, student_promo, competence_id, competence_name]
    args = sys.argv[1:]
    if len(args) != 6:
        print("Needed parameters: student_id student_surname student_name student_promo competence_id competence_name")
        return

    stud_id = args[0]
    stud_surname = args[1]
    stud_name = args[2]
    stud_promo = args[3]
    comp_id = args[4]
    comp_text = args[5]    

    image_name = "target/classes/scripts/ImageGeneration/Final/" + stud_id + "-" + comp_id
    json_dataTxId_filename = f"{image_name}.png_dataTxId_tmp.json"

    with open(json_dataTxId_filename, 'r') as openfile:
        json_object = json.load(openfile)

    dataTxId = json_object[0]
    image_desciption = f"NFT attesting that {stud_surname} {stud_name} has the competence: {comp_text}."
    image_url = f"https://arweave.net/{dataTxId}"

    json_infos = {
        "attributes":
        {
            "Name": stud_name,
            "Surname": stud_surname,
            "Promotion": stud_promo,
            "Competence": comp_text
        },
    "description": image_desciption, 
    "image": image_url, 
    "name": image_name
    }

    with open(f"{image_name}.json", "w") as outfile:
        json.dump(json_infos, outfile)
    
    if os.path.exists(json_dataTxId_filename):
        os.remove(json_dataTxId_filename)
    #if os.path.exists(image_name+".png"):
    #    os.remove(image_name+".png")

    return image_url

if __name__ == '__main__':
    main()