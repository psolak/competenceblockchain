Use the three following script as in the following exemple:




- CompetenceImageGenerator.py :
    Generate the competence image with the provided informations

    python CompetenceImageGenerator.py <student_id> <student_surname> <student_name> <competence_id> <competence_text>
    -> OUTPUT : <student_id>-<competence_id>.png

    python CompetenceImageGenerator.py 123123 John Doe 2042 456456 "Mener à bien une négociation"
    -> OUTPUT : 123123-456456.png

- CompetenceArdriveUpload.sh
    Upload the image on ardrive and generate a json file with the data transaction id.

    ./CompetenceArdriveUpload.sh <student_id>-<competence_id>.png
    -> OUTPUT : <student_id>-<competence_id>.png_dataTxId_tmp.json

    ./CompetenceArdriveUpload.sh 123123-456456.png
    -> OUTPUT : <123123>-<456456>.png_dataTxId_tmp.json

- CompetenceJsonGenerator
    Generate the json file that descripe the competence and hold the arweave link to the competence image. 
    Delete the <student_id>-<competence_id>.png_dataTxId_tmp.json file.

    python CompetenceJsonGenerator.py <student_id> <student_surname> <student_name> <competence_id> <competence_text>
    -> OUTPUT : <student_id>-<competence_id>.json

    python CompetenceJsonGenerator.py 123123 John Doe 2042 456456 "Mener à bien une négociation"
    -> OUTPUT : 123123-456456.json