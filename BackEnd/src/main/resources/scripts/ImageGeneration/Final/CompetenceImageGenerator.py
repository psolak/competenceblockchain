#!/bin/python

from email.policy import default
from PIL import Image, ImageFont, ImageDraw, ImageOps
import textwrap, sys


def main():
    # [student_id, student_surname, student_name, student_promo, competence_id, competence_name]
    args = sys.argv[1:]
    if len(args) != 6:
        print("Needed parameters: student_id student_surname student_name student_promo competence_id competence_name")
        return

    stud_id = args[0]
    stud_surname = args[1]
    stud_name = args[2]
    stud_promo = args[3]
    comp_id = args[4]
    comp_text = args[5]
    stud_text = stud_surname + " " + stud_name
    stud_text = textwrap.fill(stud_text, 18)
    comp_text = textwrap.fill(comp_text, 16)
    image_name = "target/classes/scripts/ImageGeneration/Final/" + stud_id + "-" + comp_id
    
    default_img = Image.open("target/classes/scripts/ImageGeneration/Final/competence.png")
    comp_img = default_img.copy()
    stud_font = ImageFont.truetype(font="target/classes/scripts/ImageGeneration/Final/Merriweather/Merriweather-Regular.ttf", size=200)
    comp_font = ImageFont.truetype(font="target/classes/scripts/ImageGeneration/Final/Merriweather/Merriweather-Regular.ttf", size=275)
    txt_stud_draw = ImageDraw.Draw(comp_img)
    txt_stud_draw.text((1600, 1500), stud_text, font=stud_font, fill=(228,220,203,255))
    txt_stud_draw.text((1700, 2075), stud_promo, font=stud_font, fill=(228,220,203,255))
    txt_stud_draw.text((2250, 3150), comp_text, font=comp_font, align="center", anchor="mm", fill=(10,10,51,255))

    comp_img.save(image_name + ".png")

if __name__ == '__main__':
    main()