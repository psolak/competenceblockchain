#!/bin/sh


WALLET=./walletKey.pem
OWNER_ADDRESS="erd180jt9z7tuy52rjvuplf68yfsty80u2amw4f4syggwhswm6yn0juqx8trs5"
RECEIVER_ADDRESS=$1

API_URL="https://devnet-api.elrond.com"

API_TICKER_LIST=$(curl $API_URL'/accounts/erd180jt9z7tuy52rjvuplf68yfsty80u2amw4f4syggwhswm6yn0juqx8trs5/nfts' | jq -r '.[].collection')
API_NONCE_LIST=$(curl $API_URL'/accounts/erd180jt9z7tuy52rjvuplf68yfsty80u2amw4f4syggwhswm6yn0juqx8trs5/nfts' | jq -r '.[].nonce')


COLLECTION_NAME="epitest"
TICKER="EPTT-e5a194"

HASH="0x"

RECEIVER_ADDRESS_HASH=$HASH$(erdpy wallet bech32 --decode $RECEIVER_ADDRESS)


QUANTITY=1
GAS_LIMIT=70000000

MY_NONCE=$2


for TICKER in $API_TICKER_LIST;
do

    read NONCE __ <<< "$API_NONCE_LIST"
    TICKER_HASH="$HASH$(echo -n $TICKER | xxd -p -u | tr -d \\n)"

    echo $NONCE
    echo $TICKER

    erdpy contract call $OWNER_ADDRESS --function ESDTNFTTransfer --arguments $TICKER_HASH $NONCE $QUANTITY $RECEIVER_ADDRESS_HASH --nonce $MY_NONCE --gas-limit $GAS_LIMIT --pem $WALLET --send

    MY_NONCE=$(($MY_NONCE+1))
    API_NONCE_LIST=$(echo $API_NONCE_LIST | sed 's/[^ ]* //')
done


#erdpy contract call $OWNER_ADDRESS --function ESDTNFTCreate --arguments $TICKER_HASH $QUANTITY $NFT_NAME_HASH $ROYALTIES $IMAGE_HASH $ATTRIBUTE $ARWEAVE_HASH  --recall-nonce --gas-limit $GAS_LIMIT --pem $WALLET --send

#erdpy contract call $OWNER_ADDRESS --function ESDTNFTTransfer --arguments $TICKER_HASH $QUANTITY $NONCE $RECEIVER_ADDRESS_HASH --recall-nonce --gas-limit $GAS_LIMIT --pem $WALLET --send
