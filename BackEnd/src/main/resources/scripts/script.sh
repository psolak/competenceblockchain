#!/bin/sh

WALLET=./walletKey.pem
OWNER_ADDRESS="erd180jt9z7tuy52rjvuplf68yfsty80u2amw4f4syggwhswm6yn0juqx8trs5"


API_URL="https://devnet-api.elrond.com"


HASH="0x"

OWNER_ADDRESS_HASH=$HASH$(erdpy wallet bech32 --decode $OWNER_ADDRESS)

COLLECTION_NAME="epitest"
TICKER="EPTT-e5a194"

NFT_NAME=$1
NFT_NAME_HASH=$HASH"$(echo -n $NFT_NAME | xxd -p -u | tr -d \\n)"

COLLECTION_HASH=$HASH"$(echo -n $COLLECTION_NAME | xxd -p -u | tr -d \\n)"
TICKER_HASH="$HASH$(echo -n $TICKER | xxd -p -u | tr -d \\n)"

IMAGE="./mayeul.png"
IMAGE_HASH=$HASH"$(openssl dgst -sha256 $IMAGE | awk '{print $NF}')"

ARWEAVE_ADRESS=$2
#ARWEAVE_ADRESS="https://arweave.net/f5QEfLprmdIhObl8Lf6sId8oyekyqXz5uyMGGGp7qqk"
ARWEAVE_HASH=$HASH"$(echo -n $ARWEAVE_ADRESS | xxd -p -u | tr -d \\n)"

QUANTITY=1
ROYALTIES=0
ATTRIBUTE=0
GAS_LIMIT=70000000

erdpy contract call $OWNER_ADDRESS --function ESDTNFTCreate --arguments $TICKER_HASH $QUANTITY $NFT_NAME_HASH $ROYALTIES $IMAGE_HASH $ATTRIBUTE $ARWEAVE_HASH --nonce $3 --gas-limit $GAS_LIMIT --pem $WALLET --send
sleep(30)
erdpy contract call $OWNER_ADDRESS --function ESDTNFTTransfer --arguments $TICKER_HASH $NONCE $QUANTITY $RECEIVER_ADDRESS_HASH --nonce $(($3+1)) --gas-limit $GAS_LIMIT --pem $WALLET --send

#erdpy contract call erd1qqqqqqqqqqqqqqqpqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqzllls8a5w6u --function setSpecialRole --arguments $TICKER_HASH $RECEIVER_ADDRESS_HASH 0x45534454526f6c654e4654437265617465 --recall-nonce --gas-limit 60000000 --pem $WALLET --send
