package com.ice.competenceBlockchain.database;

import com.ice.competenceBlockchain.domain.Competence;
import com.ice.competenceBlockchain.domain.ICompetenceRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JPACompetenceRepository extends JpaRepository<Competence, Long>, ICompetenceRepository {

}
