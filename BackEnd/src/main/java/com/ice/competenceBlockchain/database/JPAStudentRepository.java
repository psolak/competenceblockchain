package com.ice.competenceBlockchain.database;

import com.ice.competenceBlockchain.domain.IStudentRepository;
import com.ice.competenceBlockchain.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JPAStudentRepository extends JpaRepository<Student, Long>, IStudentRepository {
}
