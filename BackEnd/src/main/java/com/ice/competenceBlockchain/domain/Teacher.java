package com.ice.competenceBlockchain.domain;

import javax.persistence.*;

@Entity
public class Teacher {

    @Id
    private long id;

    private String firstName;
    private String lastName;

    private int rights;
    private String wallet_public_key;
    private String mail;


    Teacher(){}

    Teacher(String firstName, String lastName, int rights, String wallet_public_key, String mail){
        this.firstName = firstName;
        this.lastName = lastName;
        this.rights = rights;
        this.wallet_public_key = wallet_public_key;
        this.mail = mail;
    }

    public int getRights() {
        return rights;
    }

    public String getWalletPublicKey() {
        return wallet_public_key;
    }

    public String getMail() {
        return mail;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public long getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setRights(int rights) {
        this.rights = rights;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setWalletPublicKey(String wallet_public_key) {
        this.wallet_public_key = wallet_public_key;
    }

}
