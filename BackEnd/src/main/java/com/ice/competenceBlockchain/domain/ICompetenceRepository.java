package com.ice.competenceBlockchain.domain;

import java.util.List;
import java.util.Optional;

public interface ICompetenceRepository {
    List<Competence> findAll();
    Optional<Competence> findById(long competenceId);

    Optional<Competence> findByName(String name);
}
