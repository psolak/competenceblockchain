package com.ice.competenceBlockchain.domain;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@Entity
public class Student {

    @Id
    private long id;

    private String firstName;
    private String lastName;

    private int rights;
    private String wallet_public_key;
    private String login_epita;
    private String mail;
    private int promo;
    private String triade_2;
    private String triade_1;

    @OneToMany(mappedBy = "student", fetch = FetchType.EAGER)
    List<StudentCompetence> competences;

    Student(){}

    Student(String firstName, String lastName, int rights, String wallet_public_key, String login_epita, String mail, int promo, String triade_1, String triade_2){
        this.firstName = firstName;
        this.lastName = lastName;
        this.rights = rights;
        this.wallet_public_key = wallet_public_key;
        this.login_epita = login_epita;
        this.mail = mail;
        this.promo = promo;
        this.triade_1 = triade_1;
        this.triade_2 = triade_2;
    }

    public int getRights() {
        return rights;
    }

    public String getWalletPublicKey() {
        return wallet_public_key;
    }

    public String getMail() {
        return mail;
    }

    public String getTriade1() {
        return triade_1;
    }

    public String getTriade2() {
        return triade_2;
    }

    public int getPromo() {
        return promo;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public long getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setRights(int rights) {
        this.rights = rights;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setTriade1(String triade_1) {
        this.triade_1 = triade_1;
    }

    public void setTriade2(String triade_2) {
        this.triade_2 = triade_2;
    }

    public void setPromo(int promo) {
        this.promo = promo;
    }

    public void setWalletPublicKey(String wallet_public_key) {
        this.wallet_public_key = wallet_public_key;
    }

    public void addCompetence(Competence competence) {
        StudentCompetence studentCompetence = new StudentCompetence(
                new StudentCompetenceKey(this.getId(), competence.getId()), Date.from(Instant.now()));
        competences.add(studentCompetence);
    }

    public List<StudentCompetence> getCompetences() {
        return competences;
    }

    public String getLoginEpita() {
        return login_epita;
    }

    public void setLoginEpita(String login_epita) {
        this.login_epita = login_epita;
    }
}
