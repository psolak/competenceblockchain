package com.ice.competenceBlockchain.domain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class CompetenceService {

    @Autowired
    IStudentRepository studentRepository;

    @Autowired
    ICompetenceRepository competenceRepository;

    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    public Student getStudentFromLogin(String login) {
        return studentRepository.findByLogin(login).orElseThrow();
    }

    public List<Competence> getAllComptetences() {
        return competenceRepository.findAll();
    }

    @Transactional
    public void addCompetencesToStudent(long studentId, List<Long> competenceIds) throws JSONException, RuntimeException, IOException, InterruptedException {
        Student student = studentRepository.findById(studentId).orElseThrow();

        List<Competence> competences = new ArrayList<>();
        String path = CompetenceService.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        int nonce = 0;



        Process pn = new ProcessBuilder(path + "scripts/nonce.sh").start();
        String result = new String(pn.getInputStream().readAllBytes());
        nonce = Integer.parseInt(result);

        for (long id : competenceIds) {
            System.out.println(nonce);
            Competence competence = competenceRepository.findById(id).orElseThrow();

                Process imageGenerator = new ProcessBuilder(path + "scripts/ImageGeneration/Final/CompetenceImageGenerator.py", student.getLoginEpita(),
                        student.getLastName(), student.getFirstName(), Integer.toString(student.getPromo()), Long.toString(id), competence.getName()).start();

                result = new String(imageGenerator.getErrorStream().readAllBytes());
                System.out.println(result);

                Thread.sleep(5000);


                Process arweaveUpload = new ProcessBuilder(path + "scripts/ImageGeneration/Final/CompetenceArdriveUpload.sh", "target/classes/scripts/ImageGeneration/Final/" + student.getLoginEpita() + '-' + id + ".png").start();
                result = new String(arweaveUpload.getInputStream().readAllBytes());
                System.out.println(result);

            Thread.sleep(5000);


                Process jsonGenerator = new ProcessBuilder(path + "scripts/ImageGeneration/Final/CompetenceJsonGenerator.py", student.getLoginEpita(),
                        student.getLastName(), student.getFirstName(), Integer.toString(student.getPromo()), Long.toString(id), competence.getName()).start();

                result = new String(jsonGenerator.getErrorStream().readAllBytes());
                System.out.println(result);

            Thread.sleep(5000);

                Process arweaveJSONUpload = new ProcessBuilder(path + "scripts/ImageGeneration/Final/CompetenceArdriveUpload.sh", "target/classes/scripts/ImageGeneration/Final/" + student.getLoginEpita() + '-' + id + ".json").start();
                result = new String(arweaveJSONUpload.getInputStream().readAllBytes());
                System.out.println(result);

            Thread.sleep(5000);


            String jsonString;
                jsonString = new String(Files.readAllBytes(Paths.get(path + "scripts/ImageGeneration/Final/" + student.getLoginEpita() + "-" + id + ".json_dataTxId_tmp.json")));

            System.out.println(jsonString);


            JSONArray arr = new JSONArray(jsonString);
            String image_path = "";

            image_path = "https://arweave.net/" + arr.getString(0);



            System.out.println(image_path);


            competences.add(competence);
            student.addCompetence(competence);

            Process p = new ProcessBuilder(path + "scripts/script.sh", competence.getName(), image_path, Integer.toString(nonce)).start();
            result = new String(p.getInputStream().readAllBytes());
            System.out.println(result);
            Thread.sleep(10000);

            nonce++;

            Process transfer = new ProcessBuilder(path + "scripts/transfer.sh", student.getWalletPublicKey(), Integer.toString(nonce)).start();
            result = new String(transfer.getInputStream().readAllBytes());
            System.out.println(result);

            nonce++;
        }

    }

    public Competence getCompetenceFromName(String name) {
        return competenceRepository.findByName(name).orElseThrow();
    }

    public List<StudentCompetence> getCompetenceFromStudent(String name) {
        Student student = studentRepository.findByLastName(name).orElseThrow();

        return student.getCompetences();
    }
}
