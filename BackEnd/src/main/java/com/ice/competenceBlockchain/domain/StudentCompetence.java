package com.ice.competenceBlockchain.domain;


import javax.persistence.*;
import java.util.Date;

@Entity
public class StudentCompetence {

    @EmbeddedId
    StudentCompetenceKey id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @MapsId("studentId")
    @JoinColumn(name = "student_id")
    Student student;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @MapsId("competenceId")
    @JoinColumn(name = "competence_id")
    Competence competence;

    Date date;

    public StudentCompetence() {
    }

    public StudentCompetence(StudentCompetenceKey id, Date date) {
        this.id = id;
        this.date = date;
    }

    public Competence getCompetence() {
        return competence;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
