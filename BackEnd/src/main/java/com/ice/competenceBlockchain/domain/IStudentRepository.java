package com.ice.competenceBlockchain.domain;

import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface IStudentRepository {
    Student save(Student student);
    List<Student> findAll();
    Optional<Student> findByLastName(String lastName);
    Optional<Student> findByLogin(String login);
    Optional<Student> findById(long studentId);
}
