package com.ice.competenceBlockchain.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class StudentCompetenceKey implements Serializable {

    Long studentId;
    Long competenceId;

    public StudentCompetenceKey() {
    }

    public StudentCompetenceKey(Long studentId, Long competenceId) {
        this.studentId = studentId;
        this.competenceId = competenceId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getCompetenceId() {
        return competenceId;
    }

    public void setCompetenceId(Long competenceId) {
        this.competenceId = competenceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentCompetenceKey studentCompetenceKey = (StudentCompetenceKey) o;
        return studentId.equals(studentCompetenceKey.studentId) &&
                competenceId.equals(studentCompetenceKey.competenceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentId, competenceId);
    }
}
