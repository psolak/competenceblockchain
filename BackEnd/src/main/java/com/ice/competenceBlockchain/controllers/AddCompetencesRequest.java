package com.ice.competenceBlockchain.controllers;

import java.util.List;

public class AddCompetencesRequest {

    List<Long> competenceIds;
    Long studentId;

    public AddCompetencesRequest() {
    }

    public AddCompetencesRequest(List<Long> competenceIds, Long studentId) {
        this.competenceIds = competenceIds;
        this.studentId = studentId;
    }

    public List<Long> getCompetenceIds() {
        return competenceIds;
    }

    public void setCompetenceIds(List<Long> competenceIds) {
        this.competenceIds = competenceIds;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }
}
