package com.ice.competenceBlockchain.controllers;

import com.ice.competenceBlockchain.domain.Competence;
import com.ice.competenceBlockchain.domain.CompetenceService;
import com.ice.competenceBlockchain.domain.Student;
import com.ice.competenceBlockchain.domain.StudentCompetence;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class StudentCompetenceController {

    @Autowired
    private CompetenceService competenceService;

    @GetMapping("/api/student")
    public List<Student> getAllStudents() {
        return competenceService.getAllStudents();
    }

    @GetMapping("/api/student/{login}")
    public Student getStudentFromLogin(@PathVariable String login) {
        Student student = competenceService.getStudentFromLogin(login);
        return student;
    }

    @GetMapping("/api/competence")
    public List<Competence> getAllCompetences() {
        return competenceService.getAllComptetences();
    }

    @GetMapping("/api/competence/{name}")
    public Competence getCompetenceFromName(@PathVariable String name) {
        return competenceService.getCompetenceFromName(name);
    }

    @GetMapping("/api/student/{name}/competence")
    public List<StudentCompetence> getCompetencesFromStudent(@PathVariable String name) {
        return competenceService.getCompetenceFromStudent(name);
    }

    @PostMapping("/api/student/add")
    public void addCompetencesToStudent(@RequestBody AddCompetencesRequest request) throws JSONException, IOException, InterruptedException {
        System.out.println("start: ok");
        competenceService.addCompetencesToStudent(request.getStudentId(), request.getCompetenceIds());
    }
}