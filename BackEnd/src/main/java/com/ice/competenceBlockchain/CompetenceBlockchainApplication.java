package com.ice.competenceBlockchain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompetenceBlockchainApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompetenceBlockchainApplication.class, args);
	}

}
