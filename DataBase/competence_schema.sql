DROP TABLE IF EXISTS "Apprentissage" CASCADE;
DROP TABLE IF EXISTS "ApprentissageCours" CASCADE;
DROP TABLE IF EXISTS "Cours" CASCADE;
DROP TABLE IF EXISTS "Compétences" CASCADE;
DROP TABLE IF EXISTS "Intervenant" CASCADE;
DROP TABLE IF EXISTS "Contexte" CASCADE;



CREATE TABLE public."Apprentissage" (
    "Nom" character varying(500) NOT NULL,
    "Exemple" character varying(500),
    "Validable" boolean NOT NULL,
    "Critique" boolean NOT NULL,
    "Description" character varying(500),
    "Niveau" character varying(250) NOT NULL,
    "Compétence" character varying(250) NOT NULL,
    "Semestre" bigint NOT NULL,
    "UUID" serial NOT NULL,
    "link" character varying(500)
);


CREATE TABLE "ApprentissageCours" (
    "Nom Apprentissage" character varying(500) NOT NULL,
    "Nom Cours" character varying(500) NOT NULL
);

CREATE TABLE "Compétences" (
    "Nom contexte" character varying(500) NOT NULL
);

CREATE TABLE "Cours" (
    "Nom du cours" character varying(500) NOT NULL,
    "Intervenant" character varying(500)
);


CREATE TABLE  "Intervenant" (
    "Nom" character varying(250) NOT NULL
);

ALTER TABLE ONLY "Apprentissage"
    ADD CONSTRAINT "Compétences #1_pkey" PRIMARY KEY ("Nom");

ALTER TABLE ONLY "Compétences"
    ADD CONSTRAINT "Contexte_pkey" PRIMARY KEY ("Nom contexte");

ALTER TABLE ONLY "Cours"
    ADD CONSTRAINT "Cours_pkey" PRIMARY KEY ("Nom du cours");

ALTER TABLE ONLY "Intervenant"
    ADD CONSTRAINT "Intervenant_pkey" PRIMARY KEY ("Nom");

ALTER TABLE ONLY "Apprentissage"
    ADD CONSTRAINT "Contexte" FOREIGN KEY ("Compétence") REFERENCES  "Compétences"("Nom contexte") NOT VALID;

ALTER TABLE ONLY "ApprentissageCours"
    ADD CONSTRAINT "Nom apprentissage" FOREIGN KEY ("Nom Apprentissage") REFERENCES  "Apprentissage"("Nom");

ALTER TABLE ONLY "ApprentissageCours"
    ADD CONSTRAINT "Nom cours" FOREIGN KEY ("Nom Cours") REFERENCES  "Cours"("Nom du cours");

ALTER TABLE ONLY "Cours"
    ADD CONSTRAINT "Nom intervenant" FOREIGN KEY ("Intervenant") REFERENCES  "Intervenant"("Nom") NOT VALID;

