DROP TABLE IF EXISTS student CASCADE;
DROP TABLE IF EXISTS competence CASCADE;
DROP TABLE IF EXISTS student_competence CASCADE;
DROP TABLE IF EXISTS teacher CASCADE;
DROP TABLE IF EXISTS portfolio CASCADE;
DROP TABLE IF EXISTS project CASCADE;
DROP TABLE IF EXISTS student_project CASCADE;

CREATE TABLE student (
    id SERIAL NOT NULL,
    first_name VARCHAR(64) NOT NULL,
    last_name VARCHAR(64) NOT NULL,
    rights INT NOT NULL,
    wallet_public_key VARCHAR(128) NOT NULL,
    login_epita VARCHAR(64) NOT NULL,
    mail VARCHAR(64) NOT NULL,
    promo INT NOT NULL,
    triade_1 VARCHAR(64) NOT NULL,
    triade_2 VARCHAR(64) NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE teacher (
    id SERIAL NOT NULL,
    first_name VARCHAR(64) NOT NULL,
    last_name VARCHAR(64) NOT NULL,
    rights INT NOT NULL,
    wallet_public_key VARCHAR(128) NOT NULL,
    mail VARCHAR(64) NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE competence (
    id SERIAL NOT NULL,
    name TEXT NOT NULL,
    description TEXT NOT NULL,

    PRIMARY KEY (id)
);


CREATE TABLE portfolio (
    id SERIAL NOT NULL,
    student_id SERIAL NOT NULL,
    profil_picture_path VARCHAR(128) NOT NULL,
    cv_path VARCHAR(128) NOT NULL,
    portfolio_description TEXT NOT NULL,

    PRIMARY KEY (id),

    CONSTRAINT fk_student
    FOREIGN KEY (student_id)
    REFERENCES student(id)
);

CREATE TABLE project (
    id SERIAL NOT NULL,
    project_path VARCHAR(128) NOT NULL,
    project_description TEXT NOT NULL,
    contract_address VARCHAR(128) NOT NULL,
    
    PRIMARY KEY (id)
);

CREATE TABLE student_project (
    id SERIAL NOT NULL,
    project_id SERIAL NOT NULL,
    student_id SERIAL NOT NULL,

    PRIMARY KEY (id),

    CONSTRAINT fk_project
    FOREIGN KEY (project_id)
    REFERENCES project(id),

    CONSTRAINT fk_student
    FOREIGN KEY (student_id)
    REFERENCES student(id)
);