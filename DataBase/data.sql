INSERT INTO student(first_name, last_name, rights, wallet_public_key, login_epita, mail, promo, triade_1, triade_2)
VALUES
('Anthony', 'Chiche', 0, 'erd1t40przvexktr4drrpx95e0hmg0asu0uvwsj7qazklvywkwy4vkxq27lp3c',  'anthony.chiche', 'anthony.chiche@epita.fr', 2023, 'Esterlyn', 'Bucherons'),
('Baptiste', 'Gho', 0, 'erd10z82mehwnlke2pfe0n8rqn9cvk0e7qg9v88h6aw5zkg2att2dthsm659de',  'baptiste.gho', 'baptiste.gho@epita.fr', 2023, 'Esterlyn', 'Horia'),
('Cédric', 'Elain', 0, 'erd1fe2qe483avar6c0ag4kg4mjdftxherl25all790zhuqu6f3zu27q38qtqr',  'cedric.elain', 'cedric.elain@epita.fr', 2023, 'Pulsar', 'Horia'),
('Corentin', 'Cochard', 0, 'erd1lux4tnrujsg89s4p5xlw3pv9hzwvks92c8uttdyk6vsywkwz7y2qj2ahck',  'corentin.cochard', 'corentin.cochard@epita.fr', 2023, 'Aegis', 'Horia'),
('Florent', 'Gibart', 0, 'erd16v0e4yw9dfa3dw29qkrjg5yqpcf3rxkwp76mcd3f6tt3en3hufuq8adapq',  'florent.gibart', 'florent.gibart@epita.fr', 2023, 'Aegis', 'Phoenix'),
('Gabriel', 'Anne', 0, 'erd1f4glvq48md2xxm2c9073sc578g5ylw209rk265h3ghkw4hw6y5kq0x3yla',  'gabriel.anne', 'gabriel.anne@epita.fr', 2023, 'Esterlyn', 'Bucherons'),
('Paul', 'Beaunieux', 0, 'erd1hvverhnhr5q79pr2h3dz3yq5c9wjjs2tt5almh4zdpvkxpt9h7cqnumuzq',  'paul.beaunieux', 'paul.beaunieux@epita.fr', 2023, 'Pulsar', 'Dongs'),
('Sam', 'Gwynne', 0, 'erd12rwp7ymwgskrp0wanwvvtt8v70sdljjntmj9rcsrhfd4c9y3p2eqrsct39',  'sam.gwynne', 'sam.gwynne@epita.fr', 2023, 'Valhalla', 'Phoenix'),
('Thien', 'Phuoc', 0, 'erd1v6rjqxuuf5ryls9fp8q6ly6g03d6zpqnj4e04u6ux9ku3pkrzlcqpas4up',  'thien-phuoc.le-ngoc', 'thien-phuoc.le-ngoc@epita.fr', 2023, 'Valhalla', 'Dongs'),
('Alexandre', 'Echallier', 0, '', 'alexandre.echallier', 'alexandre.echallier@epita.fr' , 2023, 'Aegis', 'Bucherons'),
('Alexis', 'Boyadjian', 0, '', 'alexis.boyadjian','alexis.boyadjian@epita.fr', 2023, 'Valhalla', 'Phoenix'),
('Alexis', 'Vanhalle', 0, '', 'alexis.vanhalle','alexis.vanhalle@epita.fr', 2023, 'Esterlyn', 'Horia'),
('Anthony', 'Truong', 0, '', 'anthony1.truong','anthony1.truong@epita.fr', 2023, 'Valhalla', 'Dongs'),
('Antoine', 'Petit', 0, '', 'antoine.petit','antoine.petit@epita.fr', 2023, 'Valhalla', 'Dongs'),
('Briac', 'Herve', 0, '', 'briac.herve','briac.herve@epita.fr', 2023, 'Pulsar', 'Phoenix'),
('Cécile', 'Philippo', 0, '', 'cecile.philippo','cecile.philippo@epita.fr', 2023, 'Esterlyn', 'Phoenix'),
('Edwin', 'Lagant', 0, '', 'edwin.lagant','edwin.lagant@epita.fr', 2023, 'Pulsar', 'Horia'),
('Elliot', 'Caddick', 0, '', 'elliot.caddick','elliot.caddick@epita.fr', 2023, 'Pulsar', 'Bucherons'),
('Etienne', 'Samatiallian', 0, '', 'etienne.samatiallian','etienne.samatiallian@epita.fr', 2023, 'Valhalla', 'Phoenix'),
('Florent', 'Le Pape', 0, '', 'florent.le-pape','florent.le-pape@epita.fr', 2023, 'Valhalla', 'Bucherons'),
('Gabriel', 'Benillouche', 0, '', 'gabriel.benillouche','gabriel.benillouche@epita.fr', 2023, 'Aegis', 'Dongs'),
('Guillaume', 'Bailly', 0, '', 'guillaume.bailly','guillaume.bailly@epita.fr', 2023, 'Valhalla', 'Phoenix'),
('Hadrien', 'Serralheiro', 0, '', 'hadrien.serralheiro','hadrien.serralheiro@epita.fr', 2023, 'Esterlyn', 'Dongs'),
('Hugo', 'Belo', 0, '', 'hugo.belot-deloro', 'hugo.belot-deloro@epita.fr', 2023, 'Esterlyn', 'Phoenix'),
('Jean-Christophe', 'Siondini', 0, '', 'jean-christophe.siondini','jean-christophe.siondini@epita.fr', 2023, 'Aegis', 'Bucherons'),
('Jules', 'Jailloux', 0, '', 'jules.jailloux','jules.jailloux@epita.fr', 2023, 'Valhalla', 'Horia'),
('Lucas', 'Goëbels', 0, '', 'lucas.goebels','lucas.goebels@epita.fr', 2023, 'Pulsar', 'Bucherons'),
('Lucas', 'Hattenberger', 0, '', 'lucas.hattenberger','lucas.hattenberger@epita.fr', 2023, 'Pulsar', 'Dongs'),
('Lucas', 'Kervoal', 0, '', 'lucas.kervoal','lucas.kervoal@epita.fr', 2023, 'Pulsar', 'Phoenix'),
('Maxime', 'Delcourt', 0, '', 'maxime.delcourt','maxime.delcourt@epita.fr', 2023, 'Aegis', 'Phoenix'),
('Mohamed', 'Boulanaache', 0, '', 'mohamed.boulanaache','mohamed.boulanaache@epita.fr', 2023, 'Valhalla', ''),
('Nathanael', 'Grall', 0, '', 'nathanael.grall', 'nathanael.grall@epita.fr',  2023, 'Pulsar', 'Bucherons'),
('Néphélie', 'Lambrinidis', 0, '', 'nephelie.lambrinidis', 'nephelie.lambrinidis@epita.fr', 2023, 'Esterlyn', 'Horia'),
('Paul', 'Tauzies', 0, '', 'paul.tauzies', 'paul.tauzies@epita.fr', 2023, 'Aegis', 'Horia'),
('Quentin', 'Martinez', 0, '', 'quentin.martinez', 'quentin.martinez@epita.fr', 2023, 'Esterlyn', 'Dongs'),
('Samantha', 'Liebert', 0, '', 'samantha.liebert', 'samantha.liebert@epita.fr', 2023, 'Esterlyn', 'Bucherons'),
('Samy', 'Sekkat', 0, '', 'samy.sekkat', 'samy.sekkat@epita.fr', 2023, 'Aegis', 'Bucherons'),
('Seraïa', 'Cohen', 0, '', 'seraia.cohen', 'seraia.cohen@epita.fr', 2023, 'Pulsar', 'Horia'),
('Tanguy', 'Boyar', 0, '', 'tanguy.boyar', 'tanguy.boyar@epita.fr', 2023, 'Pulsar', 'Dongs'),
('Thanina', 'Sadoune', 0, '', 'thanina.sadoune', 'thanina.sadoune@epita.fr', 2023, 'Pulsar', 'Horia'),
('Thomas', 'Grandjacquot', 0, '', 'thomas.grandjacquot','thomas.grandjacquot@epita.fr', 2023, 'Valhalla', 'Bucherons'),
('Tom', 'Rottenfus', 0, '', 'tom.rottenfus','tom.rottenfus@epita.fr', 2023, 'Aegis', 'Phoenix'),
('Valentin', 'Robin', 0, '', 'valentin.robin','valentin.robin@epita.fr', 2023, 'Aegis', 'Dongs'),
('Vsevolod', 'Omelkov', 0, '', 'vsevolod.omelkov','vsevolod.omelkov@epita.fr', 2023, 'Esterlyn', 'Phoenix'),
('Yacine', 'Moufouki', 0, '', 'yacine.moufouki','yacine.moufouki@epita.fr', 2023, 'Aegis', 'Dongs'),
('Zouhair', 'Benayad', 0, '', 'zouhair.benayad','zouhair.benayad@epita.fr', 2023, 'Aegis', 'Horia');

INSERT INTO teacher(first_name, last_name, rights, wallet_public_key, mail)
VALUES
('Michel', 'Sasson', 0, 'erd1zfdqarlk7cevlt9zmznpr8ljmhq66pr0lh8x2tzlsnt7fhl3e0dqm72d55', 'michel.sasson@epita.fr'),
('Hélène', 'Ouyang', 0, 'erd1pt9xmsqull5yjtgr6gcjpaufgsk9c9dwyae0233yn5u9w8c03mwq6ggzzj', 'helene.ouyang@epita.fr');


INSERT INTO competence(name, description)
VALUES('Maths', 'Compter jusqu''à trois'),
      ('C', 'Ne pas segfault'),
      ('Assembly', 'Ne pas se suicider'),
      ('Java', 'Ne pas exécuter du code arbitraire dans un logger'),
      ('C++', 'Ne pas utiliser ce langage'),
      ('Confs technos', 'Ne pas s''endormir'),
      ('Ocaml', 'Ne pas demander les notes d''AFIT'),
      ('Gestion de projet', 'Ne pas dire à Michel de se taire'),
      ('Référentiels', 'Ne pas dire "heu"'),
      ('Français', 'Écrirre corectemant');

INSERT INTO portfolio(student_id, profil_picture_path, cv_path, portfolio_description)
VALUES
(23, '/path/to/file.png', '/path/to/cv.pdf', 'my description');


INSERT INTO project(project_path, project_description, contract_address)
VALUES
('https://www.notion.so/Projet-SMART-Competences-BlockChain-cf4d3e2ce49a4151a747ee7622d85314', 'Projet blockchainisation des compétences', '');